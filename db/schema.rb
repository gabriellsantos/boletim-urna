# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180618174336) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"

  create_table "bulletins", id: :integer, force: :cascade do |t|
    t.string   "dados"
    t.string   "assinatura"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "candidates", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.integer  "numero"
    t.integer  "votos"
    t.string   "foto"
    t.integer  "office_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["office_id"], name: "index_candidates_on_office_id", using: :btree
  end

  create_table "cities", id: :integer, force: :cascade do |t|
    t.string   "number"
    t.string   "name"
    t.boolean  "sede"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "envio_publics", id: :integer, force: :cascade do |t|
    t.integer  "fiscal_public_id"
    t.string   "code"
    t.integer  "bulletin_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["bulletin_id"], name: "index_envio_publics_on_bulletin_id", using: :btree
    t.index ["fiscal_public_id"], name: "index_envio_publics_on_fiscal_public_id", using: :btree
  end

  create_table "fiscal_phones", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.string   "phone_uuid"
    t.string   "secrete"
    t.boolean  "status"
    t.integer  "partner_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "phone"
    t.boolean  "generic"
    t.integer  "polling_place_id"
    t.boolean  "boletim_ok"
    t.boolean  "location_ok"
    t.boolean  "delegado"
    t.datetime "last_location_success"
    t.index ["partner_id"], name: "index_fiscal_phones_on_partner_id", using: :btree
    t.index ["polling_place_id"], name: "index_fiscal_phones_on_polling_place_id", using: :btree
  end

  create_table "fiscal_publics", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "litigations", id: :integer, force: :cascade do |t|
    t.integer  "eleitores"
    t.integer  "faltantes"
    t.integer  "numero"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "location_histories", force: :cascade do |t|
    t.integer  "fiscal_phone_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.decimal  "time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["fiscal_phone_id"], name: "index_location_histories_on_fiscal_phone_id", using: :btree
  end

  create_table "notifications", id: :integer, force: :cascade do |t|
    t.string   "description"
    t.string   "photo"
    t.string   "fiscal"
    t.integer  "partner_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "sended"
    t.boolean  "lida"
    t.integer  "user_id"
    t.index ["partner_id"], name: "index_notifications_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "office_cities", id: :integer, force: :cascade do |t|
    t.integer  "office_id"
    t.integer  "city_id"
    t.integer  "brancos"
    t.integer  "legenda"
    t.integer  "nominais"
    t.integer  "nulos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_office_cities_on_city_id", using: :btree
    t.index ["office_id"], name: "index_office_cities_on_office_id", using: :btree
  end

  create_table "offices", id: :integer, force: :cascade do |t|
    t.integer  "numero"
    t.integer  "nominais"
    t.integer  "brancos"
    t.integer  "nulos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "legenda"
    t.string   "name"
  end

  create_table "partners", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "partner_taghere"
    t.string   "manual"
    t.integer  "candidate_id"
    t.boolean  "active"
    t.index ["candidate_id"], name: "index_partners_on_candidate_id", using: :btree
  end

  create_table "polling_places", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.integer  "city_id"
    t.integer  "zone_id"
    t.decimal  "lat"
    t.decimal  "lng"
    t.integer  "sections_count"
    t.integer  "fiscal_phone_count"
    t.integer  "fiscal_phone_in_count"
    t.string   "status"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "address"
    t.integer  "number"
    t.index ["city_id"], name: "index_polling_places_on_city_id", using: :btree
    t.index ["zone_id"], name: "index_polling_places_on_zone_id", using: :btree
  end

  create_table "region_cities", id: :integer, force: :cascade do |t|
    t.integer  "region_id"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_region_cities_on_city_id", using: :btree
    t.index ["region_id"], name: "index_region_cities_on_region_id", using: :btree
  end

  create_table "regions", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.integer  "partner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_regions_on_partner_id", using: :btree
  end

  create_table "sections", id: :integer, force: :cascade do |t|
    t.string   "namn"
    t.string   "name"
    t.string   "address"
    t.string   "number"
    t.string   "latitude"
    t.string   "longitude"
    t.integer  "city_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "polling_place_id"
    t.string   "uid"
    t.index ["city_id"], name: "index_sections_on_city_id", using: :btree
    t.index ["polling_place_id"], name: "index_sections_on_polling_place_id", using: :btree
  end

  create_table "users", id: :integer, force: :cascade do |t|
    t.string   "name"
    t.string   "role"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "partner_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["partner_id"], name: "index_users_on_partner_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "votes", id: :integer, force: :cascade do |t|
    t.integer  "processo"
    t.integer  "pleito"
    t.integer  "turno"
    t.integer  "municipio"
    t.integer  "zona"
    t.integer  "secao"
    t.integer  "urna"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "office_id"
    t.integer  "votos"
    t.integer  "candidato"
    t.integer  "zone_id"
    t.integer  "city_id"
    t.integer  "section_id"
    t.index ["city_id"], name: "index_votes_on_city_id", using: :btree
    t.index ["office_id"], name: "index_votes_on_office_id", using: :btree
    t.index ["section_id"], name: "index_votes_on_section_id", using: :btree
    t.index ["zone_id"], name: "index_votes_on_zone_id", using: :btree
  end

  create_table "zone_cities", id: :integer, force: :cascade do |t|
    t.integer  "zone_id"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_zone_cities_on_city_id", using: :btree
    t.index ["zone_id"], name: "index_zone_cities_on_zone_id", using: :btree
  end

  create_table "zones", id: :integer, force: :cascade do |t|
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "number_i"
  end

  add_foreign_key "candidates", "offices"
  add_foreign_key "envio_publics", "bulletins"
  add_foreign_key "envio_publics", "fiscal_publics"
  add_foreign_key "fiscal_phones", "partners"
  add_foreign_key "fiscal_phones", "polling_places"
  add_foreign_key "location_histories", "fiscal_phones"
  add_foreign_key "notifications", "partners"
  add_foreign_key "notifications", "users"
  add_foreign_key "office_cities", "cities"
  add_foreign_key "office_cities", "offices"
  add_foreign_key "partners", "candidates"
  add_foreign_key "polling_places", "cities"
  add_foreign_key "polling_places", "zones"
  add_foreign_key "region_cities", "cities"
  add_foreign_key "region_cities", "regions"
  add_foreign_key "regions", "partners"
  add_foreign_key "sections", "cities"
  add_foreign_key "sections", "polling_places"
  add_foreign_key "users", "partners"
  add_foreign_key "votes", "cities"
  add_foreign_key "votes", "offices"
  add_foreign_key "votes", "sections"
  add_foreign_key "votes", "zones"
  add_foreign_key "zone_cities", "cities"
  add_foreign_key "zone_cities", "zones"
end
