class AddLastLocationSuccessToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_column :fiscal_phones, :last_location_success, :datetime
  end
end
