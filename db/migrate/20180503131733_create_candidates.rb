class CreateCandidates < ActiveRecord::Migration[5.0]
  def change
    create_table :candidates do |t|
      t.string :name
      t.integer :numero
      t.integer :votos
      t.string :foto
      t.references :office, foreign_key: true

      t.timestamps
    end
  end
end
