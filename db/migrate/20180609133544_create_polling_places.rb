class CreatePollingPlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :polling_places do |t|
      t.string :name
      t.string :location
      t.references :city, foreign_key: true
      t.references :zone, foreign_key: true
      t.decimal :lat
      t.decimal :lng
      t.integer :sections_count
      t.integer :fiscal_phone_count
      t.integer :fiscal_phone_in_count
      t.string :status

      t.timestamps
    end
  end
end
