class AddPartnerToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :partner, foreign_key: true
  end
end
