class CreateOfficeCities < ActiveRecord::Migration[5.0]
  def change
    create_table :office_cities do |t|
      t.references :office, foreign_key: true
      t.references :city, foreign_key: true
      t.integer :brancos
      t.integer :legenda
      t.integer :nominais
      t.integer :nulos

      t.timestamps
    end
  end
end
