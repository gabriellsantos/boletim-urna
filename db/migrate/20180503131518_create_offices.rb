class CreateOffices < ActiveRecord::Migration[5.0]
  def change
    create_table :offices do |t|
      t.integer :numero
      t.integer :nominais
      t.integer :brancos
      t.integer :nulos

      t.timestamps
    end
  end
end
