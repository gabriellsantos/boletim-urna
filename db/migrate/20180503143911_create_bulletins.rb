class CreateBulletins < ActiveRecord::Migration[5.0]
  def change
    create_table :bulletins do |t|
      t.string :dados
      t.string :assinatura

      t.timestamps
    end
  end
end
