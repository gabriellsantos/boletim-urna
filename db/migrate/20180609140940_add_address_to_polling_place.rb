class AddAddressToPollingPlace < ActiveRecord::Migration[5.0]
  def change
    add_column :polling_places, :address, :string
  end
end
