class CreateZoneCities < ActiveRecord::Migration[5.0]
  def change
    create_table :zone_cities do |t|
      t.references :zone, foreign_key: true
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
