class CreateLocationHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :location_histories do |t|
      t.references :fiscal_phone, foreign_key: true
      t.datetime :start_at
      t.datetime :end_at
      t.decimal :time

      t.timestamps
    end
  end
end
