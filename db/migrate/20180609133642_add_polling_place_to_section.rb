class AddPollingPlaceToSection < ActiveRecord::Migration[5.0]
  def change
    add_reference :sections, :polling_place, foreign_key: true
  end
end
