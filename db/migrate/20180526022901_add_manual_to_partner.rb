class AddManualToPartner < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :manual, :string
  end
end
