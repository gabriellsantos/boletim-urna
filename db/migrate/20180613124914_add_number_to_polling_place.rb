class AddNumberToPollingPlace < ActiveRecord::Migration[5.0]
  def change
    add_column :polling_places, :number, :integer
  end
end
