class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.integer :processo
      t.integer :pleito
      t.integer :turno
      t.integer :municipio
      t.integer :zona
      t.integer :secao
      t.integer :urna

      t.timestamps
    end
  end
end
