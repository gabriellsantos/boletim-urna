class AddZonaToVote < ActiveRecord::Migration[5.0]
  def change
    add_reference :votes, :zone, foreign_key: true
    add_reference :votes, :city, foreign_key: true
    add_reference :votes, :section, foreign_key: true
  end
end
