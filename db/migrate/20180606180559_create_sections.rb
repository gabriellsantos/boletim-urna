class CreateSections < ActiveRecord::Migration[5.0]
  def change
    create_table :sections do |t|
      t.string :namn
      t.string :name
      t.string :address
      t.string :number
      t.string :latitude
      t.string :longitude
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
