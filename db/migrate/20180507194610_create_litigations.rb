class CreateLitigations < ActiveRecord::Migration[5.0]
  def change
    create_table :litigations do |t|
      t.integer :eleitores
      t.integer :faltantes
      t.integer :numero

      t.timestamps
    end
  end
end
