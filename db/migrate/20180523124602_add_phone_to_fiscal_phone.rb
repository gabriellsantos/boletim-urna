class AddPhoneToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_column :fiscal_phones, :phone, :string
  end
end
