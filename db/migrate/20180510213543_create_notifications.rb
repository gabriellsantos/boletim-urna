class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :description
      t.string :photo
      t.string :fiscal
      t.references :partner, foreign_key: true

      t.timestamps
    end
  end
end
