class CreateFiscalPublics < ActiveRecord::Migration[5.0]
  def change
    create_table :fiscal_publics do |t|
      t.string :name
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
