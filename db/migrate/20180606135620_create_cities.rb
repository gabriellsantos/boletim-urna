class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string :number
      t.string :name
      t.boolean :sede

      t.timestamps
    end
  end
end
