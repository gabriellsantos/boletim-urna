class AddNumberIToZone < ActiveRecord::Migration[5.0]
  def change
    add_column :zones, :number_i, :integer
  end
end
