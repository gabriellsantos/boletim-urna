class AddPollingPlaceToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_reference :fiscal_phones, :polling_place, foreign_key: true
  end
end
