class AddDelegadoToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_column :fiscal_phones, :delegado, :boolean
  end
end
