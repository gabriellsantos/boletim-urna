class AddBoletimOkToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_column :fiscal_phones, :boletim_ok, :boolean
    add_column :fiscal_phones, :location_ok, :boolean
  end
end
