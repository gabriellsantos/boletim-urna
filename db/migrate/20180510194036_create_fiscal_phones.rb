class CreateFiscalPhones < ActiveRecord::Migration[5.0]
  def change
    create_table :fiscal_phones do |t|
      t.string :name
      t.string :phone_uuid
      t.string :secrete
      t.boolean :status
      t.references :partner, foreign_key: true

      t.timestamps
    end
  end
end
