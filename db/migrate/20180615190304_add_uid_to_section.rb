class AddUidToSection < ActiveRecord::Migration[5.0]
  def change
    add_column :sections, :uid, :string
  end
end
