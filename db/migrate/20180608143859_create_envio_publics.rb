class CreateEnvioPublics < ActiveRecord::Migration[5.0]
  def change
    create_table :envio_publics do |t|
      t.references :fiscal_public, foreign_key: true
      t.string :code
      t.references :bulletin, foreign_key: true

      t.timestamps
    end
  end
end
