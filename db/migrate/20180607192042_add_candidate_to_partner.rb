class AddCandidateToPartner < ActiveRecord::Migration[5.0]
  def change
    add_reference :partners, :candidate, foreign_key: true
  end
end
