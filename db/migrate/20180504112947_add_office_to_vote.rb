class AddOfficeToVote < ActiveRecord::Migration[5.0]
  def change
    add_reference :votes, :office, foreign_key: true
  end
end
