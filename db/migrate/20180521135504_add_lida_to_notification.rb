class AddLidaToNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :lida, :boolean
  end
end
