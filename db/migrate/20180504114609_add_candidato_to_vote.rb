class AddCandidatoToVote < ActiveRecord::Migration[5.0]
  def change
    add_column :votes, :candidato, :integer
  end
end
