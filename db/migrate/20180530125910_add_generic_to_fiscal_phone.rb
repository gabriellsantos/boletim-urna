class AddGenericToFiscalPhone < ActiveRecord::Migration[5.0]
  def change
    add_column :fiscal_phones, :generic, :boolean
  end
end
