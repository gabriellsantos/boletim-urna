class AddLegendaToOffice < ActiveRecord::Migration[5.0]
  def change
    add_column :offices, :legenda, :integer
  end
end
