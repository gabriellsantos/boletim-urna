# == Schema Information
#
# Table name: sections
#
#  id               :integer          not null, primary key
#  address          :string
#  latitude         :string
#  longitude        :string
#  name             :string
#  namn             :string
#  number           :string
#  uid              :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  city_id          :integer
#  polling_place_id :integer
#
# Indexes
#
#  index_sections_on_city_id           (city_id)
#  index_sections_on_polling_place_id  (polling_place_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (polling_place_id => polling_places.id)
#

require 'test_helper'

class SectionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
