# == Schema Information
#
# Table name: polling_places
#
#  id                    :integer          not null, primary key
#  address               :string
#  fiscal_phone_count    :integer
#  fiscal_phone_in_count :integer
#  lat                   :decimal(, )
#  lng                   :decimal(, )
#  location              :string
#  name                  :string
#  number                :integer
#  sections_count        :integer
#  status                :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  city_id               :integer
#  zone_id               :integer
#
# Indexes
#
#  index_polling_places_on_city_id  (city_id)
#  index_polling_places_on_zone_id  (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (zone_id => zones.id)
#

require 'test_helper'

class PollingPlaceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
