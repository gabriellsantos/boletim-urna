require 'test_helper'

class LitigationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @litigation = litigations(:one)
  end

  test "should get index" do
    get litigations_url
    assert_response :success
  end

  test "should get new" do
    get new_litigation_url
    assert_response :success
  end

  test "should create litigation" do
    assert_difference('Litigation.count') do
      post litigations_url, params: { litigation: { eleitores: @litigation.eleitores, faltantes: @litigation.faltantes, numero: @litigation.numero } }
    end

    assert_redirected_to litigation_url(Litigation.last)
  end

  test "should show litigation" do
    get litigation_url(@litigation)
    assert_response :success
  end

  test "should get edit" do
    get edit_litigation_url(@litigation)
    assert_response :success
  end

  test "should update litigation" do
    patch litigation_url(@litigation), params: { litigation: { eleitores: @litigation.eleitores, faltantes: @litigation.faltantes, numero: @litigation.numero } }
    assert_redirected_to litigation_url(@litigation)
  end

  test "should destroy litigation" do
    assert_difference('Litigation.count', -1) do
      delete litigation_url(@litigation)
    end

    assert_redirected_to litigations_url
  end
end
