require 'test_helper'

class PollingPlacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @polling_place = polling_places(:one)
  end

  test "should get index" do
    get polling_places_url
    assert_response :success
  end

  test "should get new" do
    get new_polling_place_url
    assert_response :success
  end

  test "should create polling_place" do
    assert_difference('PollingPlace.count') do
      post polling_places_url, params: { polling_place: { city_id: @polling_place.city_id, fiscal_phone_count: @polling_place.fiscal_phone_count, fiscal_phone_in_count: @polling_place.fiscal_phone_in_count, lat: @polling_place.lat, lng: @polling_place.lng, location: @polling_place.location, name: @polling_place.name, sections_count: @polling_place.sections_count, status: @polling_place.status, zone_id: @polling_place.zone_id } }
    end

    assert_redirected_to polling_place_url(PollingPlace.last)
  end

  test "should show polling_place" do
    get polling_place_url(@polling_place)
    assert_response :success
  end

  test "should get edit" do
    get edit_polling_place_url(@polling_place)
    assert_response :success
  end

  test "should update polling_place" do
    patch polling_place_url(@polling_place), params: { polling_place: { city_id: @polling_place.city_id, fiscal_phone_count: @polling_place.fiscal_phone_count, fiscal_phone_in_count: @polling_place.fiscal_phone_in_count, lat: @polling_place.lat, lng: @polling_place.lng, location: @polling_place.location, name: @polling_place.name, sections_count: @polling_place.sections_count, status: @polling_place.status, zone_id: @polling_place.zone_id } }
    assert_redirected_to polling_place_url(@polling_place)
  end

  test "should destroy polling_place" do
    assert_difference('PollingPlace.count', -1) do
      delete polling_place_url(@polling_place)
    end

    assert_redirected_to polling_places_url
  end
end
