require 'test_helper'

class FiscalPublicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fiscal_public = fiscal_publics(:one)
  end

  test "should get index" do
    get fiscal_publics_url
    assert_response :success
  end

  test "should get new" do
    get new_fiscal_public_url
    assert_response :success
  end

  test "should create fiscal_public" do
    assert_difference('FiscalPublic.count') do
      post fiscal_publics_url, params: { fiscal_public: { email: @fiscal_public.email, name: @fiscal_public.name, phone: @fiscal_public.phone } }
    end

    assert_redirected_to fiscal_public_url(FiscalPublic.last)
  end

  test "should show fiscal_public" do
    get fiscal_public_url(@fiscal_public)
    assert_response :success
  end

  test "should get edit" do
    get edit_fiscal_public_url(@fiscal_public)
    assert_response :success
  end

  test "should update fiscal_public" do
    patch fiscal_public_url(@fiscal_public), params: { fiscal_public: { email: @fiscal_public.email, name: @fiscal_public.name, phone: @fiscal_public.phone } }
    assert_redirected_to fiscal_public_url(@fiscal_public)
  end

  test "should destroy fiscal_public" do
    assert_difference('FiscalPublic.count', -1) do
      delete fiscal_public_url(@fiscal_public)
    end

    assert_redirected_to fiscal_publics_url
  end
end
