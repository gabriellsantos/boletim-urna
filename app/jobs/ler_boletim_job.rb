class LerBoletimJob < ApplicationJob
  queue_as :default

  def is_number?(string)
    true if Integer(string) rescue false
  end

  def perform(boletins,tipo,idFiscal)
    Bulletin.validate_and_perform(boletins,tipo,idFiscal)
  end
end
