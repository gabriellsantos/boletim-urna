json.extract! section, :id, :namn, :name, :address, :number, :latitude, :longitude, :city_id, :created_at, :updated_at
json.url section_url(section, format: :json)
