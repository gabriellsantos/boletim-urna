json.extract! city, :id, :number, :name, :sede, :created_at, :updated_at
json.url city_url(city, format: :json)
