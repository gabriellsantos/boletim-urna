json.extract! zone, :id, :number, :created_at, :updated_at
json.url zone_url(zone, format: :json)
