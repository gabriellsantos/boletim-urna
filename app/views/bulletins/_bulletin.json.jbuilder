json.extract! bulletin, :id, :dados, :assinatura, :created_at, :updated_at
json.url bulletin_url(bulletin, format: :json)
