json.extract! candidate, :id, :name, :numero, :votos, :foto, :office_id, :created_at, :updated_at
json.url candidate_url(candidate, format: :json)
