json.extract! vote, :id, :processo, :pleito, :turno, :municipio, :zona, :secao, :urna, :created_at, :updated_at
json.url vote_url(vote, format: :json)
