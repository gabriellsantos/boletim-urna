json.extract! fiscal_public, :id, :name, :phone, :email, :created_at, :updated_at
json.url fiscal_public_url(fiscal_public, format: :json)
