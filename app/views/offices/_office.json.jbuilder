json.extract! office, :id, :numero, :nominais, :brancos, :nulos, :created_at, :updated_at
json.url office_url(office, format: :json)
