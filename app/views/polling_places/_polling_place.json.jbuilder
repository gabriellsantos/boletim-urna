json.extract! polling_place, :id, :name, :location, :city_id, :zone_id, :lat, :lng, :sections_count, :fiscal_phone_count, :fiscal_phone_in_count, :status, :created_at, :updated_at
json.url polling_place_url(polling_place, format: :json)
