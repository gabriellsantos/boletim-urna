class LitigationsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_litigation, only: [:show, :edit, :update, :destroy]

  # GET /litigations
  # GET /litigations.json
  def index
    @litigations = Litigation.all
  end

  # GET /litigations/1
  # GET /litigations/1.json
  def show
  end

  # GET /litigations/new
  def new
    @litigation = Litigation.new
  end

  # GET /litigations/1/edit
  def edit
  end

  # POST /litigations
  # POST /litigations.json
  def create
    @litigation = Litigation.new(litigation_params)

    respond_to do |format|
      if @litigation.save
        format.html { redirect_to @litigation, notice: 'Litigation was successfully created.' }
        format.json { render :show, status: :created, location: @litigation }
      else
        format.html { render :new }
        format.json { render json: @litigation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /litigations/1
  # PATCH/PUT /litigations/1.json
  def update
    respond_to do |format|
      if @litigation.update(litigation_params)
        format.html { redirect_to @litigation, notice: 'Litigation was successfully updated.' }
        format.json { render :show, status: :ok, location: @litigation }
      else
        format.html { render :edit }
        format.json { render json: @litigation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /litigations/1
  # DELETE /litigations/1.json
  def destroy
    @litigation.destroy
    respond_to do |format|
      format.html { redirect_to litigations_url, notice: 'Litigation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_litigation
      @litigation = Litigation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def litigation_params
      params.require(:litigation).permit(:eleitores, :faltantes, :numero)
    end
end
