class PollingPlacesController < ApplicationController
  before_action :set_polling_place, only: [:show, :edit, :update, :destroy, :set_phone]
  before_action :authenticate_user!, except: [:by_city, :set_phone]

  # GET /polling_places
  # GET /polling_places.json
  def index
    @tag = params[:tag] || :all
    @q = PollingPlace.send(@tag).ransack(params[:q])
    @polling_places = @q.result.page(params[:page]).per(300)
  end

  def by_city
    zone = nil
    @polling_places = PollingPlace.where(city_id: params[:city])
    if @polling_places.present?
      zone = @polling_places.first.zone.number_i
    end
    render json: {places: @polling_places, zone: zone}
  end

  def set_phone
    begin
      phone = FiscalPhone.find_by_secrete(params[:codigo])
      phone.update(polling_place: @polling_place)
      render json: @polling_place
    rescue
      render json: {error: "fiscal não encontrado"}
    end
  end

  # GET /polling_places/1
  # GET /polling_places/1.json
  def show
  end

  # GET /polling_places/new
  def new
    @polling_place = PollingPlace.new
  end

  # GET /polling_places/1/edit
  def edit
  end

  # POST /polling_places
  # POST /polling_places.json
  def create
    @polling_place = PollingPlace.new(polling_place_params)

    respond_to do |format|
      if @polling_place.save
        format.html { redirect_to @polling_place, notice: 'Polling place was successfully created.' }
        format.json { render :show, status: :created, location: @polling_place }
      else
        format.html { render :new }
        format.json { render json: @polling_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /polling_places/1
  # PATCH/PUT /polling_places/1.json
  def update
    respond_to do |format|
      if @polling_place.update(polling_place_params)
        format.html { redirect_to @polling_place, notice: 'Polling place was successfully updated.' }
        format.json { render :show, status: :ok, location: @polling_place }
      else
        format.html { render :edit }
        format.json { render json: @polling_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /polling_places/1
  # DELETE /polling_places/1.json
  def destroy
    @polling_place.destroy
    respond_to do |format|
      format.html { redirect_to polling_places_url, notice: 'Polling place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def dashboard
    @places_count = PollingPlace.count
    @need_fiscal = PollingPlace.need_fiscal
    @cadastros = PollingPlace.all.sum(:fiscal_phone_count)
    @sections_count = Section.count
    @need_fiscal_in = PollingPlace.need_fiscal_in
    @deficit_grouped = @need_fiscal.group(:city).sum("sections_count - fiscal_phone_count").sort_by {|a| -a.last}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_polling_place
      @polling_place = PollingPlace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def polling_place_params
      params.require(:polling_place).permit(:name, :location, :city_id, :zone_id, :lat, :lng, :sections_count, :fiscal_phone_count, :fiscal_phone_in_count, :status)
    end
end
