class NotificationsController < ApplicationController
  before_action :authenticate_user!, except: [:receber, :ler]
  load_and_authorize_resource except: [:receber,:ler]
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  before_action :allow_iframe, :set_headers

   skip_before_action :verify_authenticity_token


  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET, POST'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  # GET /notifications
  # GET /notifications.json
  def index
    if current_user.admin?
      @notifications_recebidas = Notification.where(sended: false).order(lida: :asc)
      @notifications_enviadas = Notification.where(sended: true).order(lida: :asc)
    else
      @notifications_recebidas = Notification.where(partner: current_user.partner, sended: false).order(lida: :asc)
      @notifications_enviadas = Notification.where(partner: current_user.partner, sended: true).order(lida: :asc)
    end

  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
  end

  # GET /notifications/new
  def new
    @notification = Notification.new(partner: current_user.partner, sended: true, user:current_user)
  end

  # GET /notifications/1/edit
  def edit
  end

  def ler
    Notification.find(params[:msg]).update(lida: true)
    render json: {ok: "Lida"}
  end

  def receber
    notification = Notification.new
    phone = FiscalPhone.find_by_secrete(params[:codigo])
    notification.partner = phone.partner
    notification.sended = false
    notification.description = params[:msg]
    notification.fiscal = params[:codigo]
    notification.photo = params[:base64]
    notification.lida = false
  
    if notification.save
      render json: {retorno: "ok"}
    else
      render json: {retorno: "error"}
    end

  end

  # POST /notifications
  # POST /notifications.json
  def create
    @notification = Notification.new(notification_params)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render :show, status: :created, location: @notification }
      else
        format.html { render :new }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notifications/1
  # PATCH/PUT /notifications/1.json
  def update
    respond_to do |format|
      if @notification.update(notification_params)
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification }
      else
        format.html { render :edit }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:description, :photo, :fiscal, :partner_id, :sended, :lida, :user_id)
    end
end
