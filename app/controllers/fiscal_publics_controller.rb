class FiscalPublicsController < ApplicationController
  before_action :authenticate_user!, except: :autorizar
  load_and_authorize_resource except: :autorizar
  before_action :set_fiscal_public, only: [:show, :edit, :update, :destroy]
  before_action :allow_iframe, :set_headers

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  def autorizar
    fiscal = FiscalPublic.find_or_create_by(phone: params[:phone])
    fiscal.name = params[:name]
    fiscal.email = params[:email]
    fiscal.save
    render json: {msg: "ok", codigo: "fiscalpublico", fiscal: fiscal.id}
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  # GET /fiscal_publics
  # GET /fiscal_publics.json
  def index
    @fiscal_publics = FiscalPublic.all
  end

  # GET /fiscal_publics/1
  # GET /fiscal_publics/1.json
  def show
  end

  # GET /fiscal_publics/new
  def new
    @fiscal_public = FiscalPublic.new
  end

  # GET /fiscal_publics/1/edit
  def edit
  end

  # POST /fiscal_publics
  # POST /fiscal_publics.json
  def create
    @fiscal_public = FiscalPublic.new(fiscal_public_params)

    respond_to do |format|
      if @fiscal_public.save
        format.html { redirect_to @fiscal_public, notice: 'Fiscal public was successfully created.' }
        format.json { render :show, status: :created, location: @fiscal_public }
      else
        format.html { render :new }
        format.json { render json: @fiscal_public.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fiscal_publics/1
  # PATCH/PUT /fiscal_publics/1.json
  def update
    respond_to do |format|
      if @fiscal_public.update(fiscal_public_params)
        format.html { redirect_to @fiscal_public, notice: 'Fiscal public was successfully updated.' }
        format.json { render :show, status: :ok, location: @fiscal_public }
      else
        format.html { render :edit }
        format.json { render json: @fiscal_public.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fiscal_publics/1
  # DELETE /fiscal_publics/1.json
  def destroy
    @fiscal_public.destroy
    respond_to do |format|
      format.html { redirect_to fiscal_publics_url, notice: 'Fiscal public was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fiscal_public
      @fiscal_public = FiscalPublic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fiscal_public_params
      params.require(:fiscal_public).permit(:name, :phone, :email)
    end
end
