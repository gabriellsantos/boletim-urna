class PartnersController < ApplicationController
  before_action :authenticate_user!, except: [:ativar_aparelho,:manual,:youtubeVideo,:ativar_fiscal,:retornaParceiros,:verificar_fiscal,:verificar_fiscal_publico,:termos_condicoes,:set_polling_place, :location]
  load_and_authorize_resource except: [:ativar_aparelho,:manual,:youtubeVideo,:ativar_fiscal,:retornaParceiros,:verificar_fiscal,:verificar_fiscal_publico,:termos_condicoes,:set_polling_place, :location]
  before_action :set_partner, only: [:show, :edit, :update, :destroy]

  before_action :allow_iframe, :set_headers

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def termos_condicoes
    render layout: "clean"
  end

  def teste
    render json: Office.all.to_json
  end

  def set_polling_place

    zone = Zone.find_by_number_i(params[:zona].to_i)
    if zone
      secao = zone.sections.where(number: params[:secao]).first
      if secao
        fiscal = FiscalPhone.find_by_secrete(params[:codigo])
        fiscal.polling_place_id = secao.polling_place_id
        fiscal.save
        render json: fiscal.polling_place
      else
        render json: {ok: false} 
      end

    else
      render json: {ok: false} 
    end

  end


  def atualiza_fiscal
    fiscal = FiscalPhone.find(params[:fiscal])
    fiscal.polling_place_id = params[:local]
    fiscal.delegado = params[:delegado]
    fiscal.save
    redirect_to aparelhos_partners_path
  end




  def verificar_fiscal

    fiscal = FiscalPhone.where(secrete: params[:codigo], status: true).first
    if fiscal
      render json: {ok: true, partner: fiscal.partner.id, partner_taghere: fiscal.partner.partner_taghere || 0, codigo: fiscal.secrete, idFiscal: fiscal.id}
    else
      render json: {ok: false} 
    end

  end

  def verificar_fiscal_publico
    fiscal = FiscalPublic.find(params[:idFiscal])
    if fiscal
      render json: {ok: true, codigo: "fiscalpublico", fiscal: fiscal.id}
    else
      render json: {ok: false, codigo: "fiscalpublico", fiscal: 0}
    end
  end



  # GET /partners
  # GET /partners.json
  def index
    @partners = Partner.all
  end

  def manual
  
    fiscal = FiscalPhone.find_by_secrete(params[:codigo])

    render json: {url: fiscal.partner.manual.url}
  end


  # GET /partners/1
  # GET /partners/1.json
  def show
  end

  def aparelhos
    if current_user.partner?
      @q = FiscalPhone.where(partner: current_user.partner).where.not(polling_place_id:nil).ransack(params[:q])
    else
      @q = FiscalPhone.where.not(polling_place_id: nil).ransack(params[:q])
    end
    @aparelhos = @q.result.page(params[:page])
  end

  def youtubeVideo
    render json: {url:"https://www.youtube.com/watch?v=mjB68gTzqm8&rel=0"}
  end

  def ativar_sms
    
  end

  def ativar_aparelho
    ok = false

    aparelho = FiscalPhone.where(secrete: params[:codigo].upcase).first #, status: false
    
    if aparelho.present? and aparelho.generic
      aparelho = FiscalPhone.create(name: params[:nome], phone: params[:telefone], status: true, generic: false,partner_id: aparelho.partner.id)
    end

    if aparelho.present?
      aparelho.name = params[:nome]
      aparelho.phone = params[:telefone]
      aparelho.status = true
      ok = aparelho.save
    end

    render json: {ok: ok, partner: aparelho.partner.id, partner_taghere: aparelho.partner.partner_taghere || 0, codigo: aparelho.secrete}

  end

  def retornaParceiros
    parceiros = Partner.where(active: true)
    render json: {parceiros: parceiros, cities: City.where.not(name: nil)}
  end

  def ativar_fiscal
    ok = false
    aparelho = FiscalPhone.find_or_create_by(phone: params[:telefone])
    aparelho.partner_id = params[:partner]
    aparelho.polling_place_id = params[:polling_place_id]
    aparelho.name = params[:nome]
    aparelho.status = true
    aparelho.generic = false
    ok = aparelho.save
    render json: {ok: ok, partner: aparelho.partner.id, partner_taghere: aparelho.partner.partner_taghere || 0, codigo: aparelho.secrete, idFiscal: aparelho.id}
  end

  def novo_aparelho
    aparelho = FiscalPhone.new
    aparelho.partner_id = params[:partner].to_i
    aparelho.name = params[:name]
    aparelho.status = false
    aparelho.save

    redirect_to aparelhos_partners_path
  end

  # GET /partners/new
  def new
    @partner = Partner.new
  end

  # GET /partners/1/edit
  def edit
  end

  # POST /partners
  # POST /partners.json
  def create
    @partner = Partner.new(partner_params)

    respond_to do |format|
      if @partner.save
        format.html { redirect_to partners_path, notice: 'Partner was successfully created.' }
        format.json { render :show, status: :created, location: @partner }
      else
        format.html { render :new }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partners/1
  # PATCH/PUT /partners/1.json
  def update
    respond_to do |format|
      if @partner.update(partner_params)
        format.html { redirect_to partners_path, notice: 'Partner was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner }
      else
        format.html { render :edit }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partners/1
  # DELETE /partners/1.json
  def destroy
    @partner.destroy
    respond_to do |format|
      format.html { redirect_to partners_url, notice: 'Partner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def location
    @fiscal = FiscalPhone.find_by_secrete(params[:codigo])
    @distance = @fiscal.polling_place.distance_to([params[:latitude].to_f, params[:longitude].to_f])
    if @distance > 0.2
      @fiscal.far_way
    else
      @fiscal.check_location
    end
    render json: {status: :ok, result: @distance}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_params
      params.require(:partner).permit(:name, :description,:partner_taghere,:manual,:candidate_id)
    end
end
