class OfficesController < ApplicationController
  before_action :authenticate_user!, except: :receber
  load_and_authorize_resource except: :receber
  before_action :allow_iframe, :set_headers
  before_action :set_office, only: [:show, :edit, :update, :destroy,:apuracao]

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  # GET /offices
  # GET /offices.json
  def index
    @offices = Office.all
    #@dados_suplementares = Litigation.getDados
  end

  def apuracao_old
    @votes = @office.votes
    @candidatos = Candidate.order(votos: :desc)
    @eleito = false
    @segundoturno = false
    @pleito = Litigation.last
    @p = 0
    if @candidatos

      @p = (@pleito.eleitores - ( @office.brancos + @office.nulos + @pleito.faltantes))/2
      if @candidatos.first.votos >= @p + 1
        @eleito = true
      end
      #verifica se haverá segundo turno
      votos = @candidatos.sum{|a| a.votos} - @candidatos.first.votos
      if votos >= @p + 1
        @segundoturno = true
      end

    end
  end

  def apuracao
    render "firebase"
  end

  # GET /offices/1
  # GET /offices/1.json
  def show
  end

  def is_number?(string)
    true if Integer(string) rescue false
  end

  def receber_old
    dados = params[:data].split(" ")
    office = nil
    candidate = nil

    boletim = Bulletin.find_by_assinatura(dados.last.split(":")[1])

    if !boletim
      Bulletin.create(dados: params[:data], assinatura: dados.last.split(":")[1])

      dados.each do |d|
        if d.split(":")[0] == "CARG" #cargo
          office = Office.find_or_create_by(numero: d.split(":")[1].to_i)
        end
        if is_number?(d.split(":")[0])#candidato
          candidate = Candidate.find_or_create_by(numero: d.split(":")[0].to_i )
          candidate.office = office
          begin
            candidate.votos += d.split(":")[1].to_i
          rescue
            candidate.votos = d.split(":")[1].to_i
          end

          candidate.save
        end
        if d.split(":")[0] == "NOMI" #votos nominais
          begin
            office.nominais += d.split(":")[1].to_i
          rescue
            office.nominais = d.split(":")[1].to_i
          end

        end
        if d.split(":")[0] == "BRAN" #votos brancos
          begin
            office.brancos += d.split(":")[1].to_i
          rescue
            office.brancos = d.split(":")[1].to_i
          end

        end
        if d.split(":")[0] == "LEGC" #votos legenda
          begin
            office.legenda += d.split(":")[1].to_i
          rescue
            office.legenda = d.split(":")[1].to_i
          end

        end
        if d.split(":")[0] == "NULO" #votos nulos
          begin
            office.nulos += d.split(":")[1].to_i
          rescue
            office.nulos = d.split(":")[1].to_i
          end

          office.save
        end
      end
    else

    end

    render json: {ok: "OK"}
  end

  def receber

    #boletins = JSON.parse(params[:data])
    #binding.pry
    LerBoletimJob.perform_later(params[:data],params[:tipo],params[:idFiscal])
    render json: {ok: "OK"}
  end

  # GET /offices/new
  def new
    @office = Office.new
  end

  # GET /offices/1/edit
  def edit
  end

  # POST /offices
  # POST /offices.json
  def create
    @office = Office.new(office_params)

    respond_to do |format|
      if @office.save
        format.html { redirect_to @office, notice: 'Office was successfully created.' }
        format.json { render :show, status: :created, location: @office }
      else
        format.html { render :new }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offices/1
  # PATCH/PUT /offices/1.json
  def update
    respond_to do |format|
      if @office.update(office_params)
        format.html { redirect_to @office, notice: 'Office was successfully updated.' }
        format.json { render :show, status: :ok, location: @office }
      else
        format.html { render :edit }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offices/1
  # DELETE /offices/1.json
  def destroy
    @office.destroy
    respond_to do |format|
      format.html { redirect_to offices_url, notice: 'Office was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office
      @office = Office.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_params
      params.require(:office).permit(:numero, :nominais, :brancos, :nulos, :legenda, :name)
    end
end
