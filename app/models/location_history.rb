# == Schema Information
#
# Table name: location_histories
#
#  id              :integer          not null, primary key
#  end_at          :datetime
#  start_at        :datetime
#  time            :decimal(, )
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  fiscal_phone_id :integer
#
# Indexes
#
#  index_location_histories_on_fiscal_phone_id  (fiscal_phone_id)
#
# Foreign Keys
#
#  fk_rails_...  (fiscal_phone_id => fiscal_phones.id)
#

class LocationHistory < ApplicationRecord
  belongs_to :fiscal_phone
end
