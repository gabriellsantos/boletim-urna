# == Schema Information
#
# Table name: zone_cities
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city_id    :integer
#  zone_id    :integer
#
# Indexes
#
#  index_zone_cities_on_city_id  (city_id)
#  index_zone_cities_on_zone_id  (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (zone_id => zones.id)
#

class ZoneCity < ApplicationRecord
  belongs_to :zone
  belongs_to :city
end
