# == Schema Information
#
# Table name: partners
#
#  id              :integer          not null, primary key
#  active          :boolean
#  description     :string
#  manual          :string
#  name            :string
#  partner_taghere :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  candidate_id    :integer
#
# Indexes
#
#  index_partners_on_candidate_id  (candidate_id)
#
# Foreign Keys
#
#  fk_rails_...  (candidate_id => candidates.id)
#

class Partner < ApplicationRecord

  has_many :fiscal_phones
  has_many :notifications
  has_many :regions
  belongs_to :candidate, optional: true

  mount_uploader :manual, AvatarUploader

  def to_s
    self.name
  end
end
