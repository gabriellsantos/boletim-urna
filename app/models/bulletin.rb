# == Schema Information
#
# Table name: bulletins
#
#  id         :integer          not null, primary key
#  assinatura :string
#  dados      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Bulletin < ApplicationRecord

  def self.validate_and_perform(boletins,tipo,idFiscal)

    boletins.each do |b|
      c = b.split("QRBU")
      c.shift
      if check_hash(c)
        perform(b,tipo,idFiscal)
      end
    end

  end

  def self.is_number?(string)
    true if Integer(string) rescue false
  end

  def self.check_hash(boletins)
    dados = ""
    if boletins[0].split(":")[2].to_i != boletins.count
      return false
    end
    boletins.map do |boletim|
      p1 = boletim.split("VRQR:")
      p2 = p1[1].split(" ")[0]
      boletim.slice!("#{p1[0]}VRQR:#{p2} ")
      p3 = boletim.split(" HASH:")
      boletim_data = p3[0]
      hash = p3[1].split(" ASSI:")[0]
      sha = Digest::SHA2.new(512).hexdigest(dados + boletim_data.strip).upcase
      if sha != hash.strip
        return false
      end
      dados += boletim
    end
    true
  end

  def self.perform(boletim,tipo,idFiscal)
    #binding.pry
    #boletins.each do |boletim|
      dados = boletim.split(" ")
      office = nil
      candidate = nil
      municipio = nil
      pleito = nil
      processo = nil
      secao = nil
      turno = nil
      urna = nil
      zona = nil
      litigation = nil
      muni = nil
      se = nil
      zo = nil
      office_city = nil
      boletim_existente = Bulletin.find_by_assinatura(dados.last.split(":")[1])

      if !boletim_existente
        Bulletin.create(dados: boletim, assinatura: dados.last.split(":")[1])

        dados.each do |d|

          case d.split(":")[0]
            when "PROC"
              processo = d.split(":")[1].to_i
            when "PLEI"
              pleito = d.split(":")[1].to_i
              litigation = Litigation.find_or_create_by(numero: pleito, eleitores: 1018329)
            when "TURN"
              turno = d.split(":")[1].to_i
            when "MUNI"
              municipio = d.split(":")[1].to_i
              muni = City.find_or_create_by(number: d.split(":")[1].rjust(5,'0'))
            when "ZONA"
              zona = d.split(":")[1].to_i
              zo = Zone.find_or_create_by(number: d.split(":")[1].rjust(4,'0'))
            when "SECA"
              secao = d.split(":")[1].to_i
              se = Section.find_or_create_by(number: d.split(":")[1].rjust(4,'0'))
            when "IDUE"
              urna = d.split(":")[1].to_i
            when "FALT"
              begin
                litigation.faltantes += d.split(":")[1].to_i
              rescue
                litigation.faltantes = d.split(":")[1].to_i
              end
              litigation.save
          end


          if d.split(":")[0] == "CARG" #cargo
            office = Office.find_or_create_by(numero: d.split(":")[1].to_i)
            office_city = OfficeCity.find_or_create_by(office_id: office.id, city_id: muni.id)
          end
          if Bulletin.is_number?(d.split(":")[0])#candidato
            Vote.create(municipio: municipio, pleito: pleito, processo: processo, secao: secao, turno: turno, urna: urna, zona: zona, office_id: office.id,votos: d.split(":")[1].to_i, candidato: d.split(":")[0].to_i, city_id: muni.id, zone_id: zo.id, section_id: se.id)
          end
          if d.split(":")[0] == "NOMI" #votos nominais
            qtd = d.split(":")[1].to_i
            begin
              office.nominais += qtd #d.split(":")[1].to_i
            rescue
              office.nominais = qtd #d.split(":")[1].to_i
            end

            begin
              office_city.nominais += qtd
            rescue
              office_city.nominais = qtd
            end

          end
          if d.split(":")[0] == "BRAN" #votos brancos
            qtd = d.split(":")[1].to_i
            begin
              office.brancos += qtd #d.split(":")[1].to_i
            rescue
              office.brancos = qtd #d.split(":")[1].to_i
            end

            begin
              office_city.brancos += qtd
            rescue
              office_city.brancos = qtd
            end

          end
          if d.split(":")[0] == "LEGC" #votos legenda
            qtd = d.split(":")[1].to_i
            begin
              office.legenda += qtd #d.split(":")[1].to_i
            rescue
              office.legenda =  qtd #d.split(":")[1].to_i
            end

            begin
              office_city.legenda += qtd
            rescue
              office_city.legenda = qtd
            end

          end
          if d.split(":")[0] == "NULO" #votos nulos
            qtd = d.split(":")[1].to_i
            begin
              office.nulos += qtd #d.split(":")[1].to_i
            rescue
              office.nulos = qtd #d.split(":")[1].to_i
            end

            begin
              office_city.nulos += qtd
            rescue
              office_city.nulos = qtd
            end

            office.save
            office_city.save
          end

        end
        Bulletin.send_to_firebase
      else

      end

    #salva envio do fiscal
    if tipo == "fiscalpublico"
      codigo = EnvioPublic.find_or_create_by(fiscal_public_id: idFiscal, bulletin_id: boletim_existente.id)
    end
    #end
  end


  def self.send_to_firebase

    firebase_url = "https://notificato-seplan.firebaseio.com/"#"https://desenvolvimento-172113.firebaseio.com"
    firebase_secret = "qm4kJVOdqS4sjVVOkA2tbVC5qpIRjd9OCN5Alrs1"#"BwJRz6O0moPfJODq42pucgyFurSzWb0k6UBii5Og"

    firebase = Firebase::Client.new(firebase_url, firebase_secret)
    tag = {}
    Office.all.each do |o|
      tag["cargo_#{o.id}"] = JSON.parse(o.to_json)
    end
    #tag["pleito"] = Litigation.last.to_json
    response = firebase.update("apuracao", tag)
  end
end
