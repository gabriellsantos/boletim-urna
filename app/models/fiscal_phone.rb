# == Schema Information
#
# Table name: fiscal_phones
#
#  id                    :integer          not null, primary key
#  boletim_ok            :boolean
#  delegado              :boolean
#  generic               :boolean
#  last_location_success :datetime
#  location_ok           :boolean
#  name                  :string
#  phone                 :string
#  phone_uuid            :string
#  secrete               :string
#  status                :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  partner_id            :integer
#  polling_place_id      :integer
#
# Indexes
#
#  index_fiscal_phones_on_partner_id        (partner_id)
#  index_fiscal_phones_on_polling_place_id  (polling_place_id)
#
# Foreign Keys
#
#  fk_rails_...  (partner_id => partners.id)
#  fk_rails_...  (polling_place_id => polling_places.id)
#

class FiscalPhone < ApplicationRecord
  require "uniquify"
  belongs_to :partner,optional: true
  has_many :notifications, foreign_key: :fiscal, primary_key: :secrete
  belongs_to :polling_place,optional: true
  has_many :location_histories

  uniquify :secrete, length: 6, chars: 0..9

  default_scope {order(id: :desc)}

  after_save :count_polling

  def count_polling
    if polling_place.present?
      polling_place.fiscal_phone_count = polling_place.fiscal_phones.count
      polling_place.save
    end
  end

  def far_way
    locations = location_histories.order(id: :desc).where(end_at: nil)
    if locations.present?
      location = locations.first
      location.update(end_at: Time.now, time: ((location.start_at - Time.now)/ 1.hour).round(2))
    end
  end

  def check_location
    locations = location_histories.where(end_at: nil)
    if locations.blank?
      LocationHistory.create(fiscal_phone: self, start_at: Time.now)
    end
    self.update(last_location_success: Time.now)
  end



end
