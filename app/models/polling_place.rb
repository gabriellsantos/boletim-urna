# == Schema Information
#
# Table name: polling_places
#
#  id                    :integer          not null, primary key
#  address               :string
#  fiscal_phone_count    :integer
#  fiscal_phone_in_count :integer
#  lat                   :decimal(, )
#  lng                   :decimal(, )
#  location              :string
#  name                  :string
#  number                :integer
#  sections_count        :integer
#  status                :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  city_id               :integer
#  zone_id               :integer
#
# Indexes
#
#  index_polling_places_on_city_id  (city_id)
#  index_polling_places_on_zone_id  (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (zone_id => zones.id)
#

class PollingPlace < ApplicationRecord
  require 'csv'
  belongs_to :city
  belongs_to :zone
  has_many :sections
  has_many :fiscal_phones

  geocoded_by :location, :latitude  => :lat, :longitude => :lng
  #after_validation :geocode



  def as_json(options = {})
    s = super(options)
    s[:secoes] = self.sections
    s
  end

  def to_s
    self.name
  end

  def self.import_places
    file = File.read("#{Rails.root}/lib/assets/locais.json")
    list = JSON.parse(file, symbolize_names: true)
    Zone.all.map do |zone|
      places = list[:"#{zone.number_i}"]

      places.map do |place|
        city_name = place[:name].split(" - ").last
        pp = PollingPlace.new(zone: zone)
        pp.city = City.find_by_name(city_name)
        pp.name = place[:name]
        pp.location = "#{place[:name]}, #{city_name}, Tocantins, Brasil"
        pp.fiscal_phone_count = 0
        pp.fiscal_phone_in_count = 0
        pp.status = :incompleta
        pp.address = "#{place[:address]}, #{city_name}, Tocantins, Brasil"
        pp.save

        place[:numbers].split(", ").map do |number|
          section = Section.create(number: number, latitude: pp.lat, longitude: pp.lng, polling_place: pp )
        end
        pp.sections_count = pp.sections.count
        pp.save
        pp
      end
    end
  end

  def self.fix_location_by(field)
    PollingPlace.where(lat:nil).map do |pp|
      pp.location = pp.send(field)
      pp.save
    end
  end


  def self.import_by_csv
    r = 0
    CSV.foreach("#{Rails.root}/lib/assets/locais.csv", :headers => true, col_sep: ";", encoding: 'windows-1251:utf-8') do |row|
      r += 1
      begin
        z = Zone.find_by_number_i(row["ZONA"])
        s = Section.all.joins(:polling_place).where(polling_places:{zone_id: z.id}).first
        p = s.polling_place
        p.number = row["NUM_LOCAL"]
        p.address = "#{row["ENDERECO"]}, #{row["BAIRRO_LOCAL_VOT"]}, #{row["CEP"]}, #{p.city.name}, Tocantins, Brasil"
        p.save
      rescue
        puts r
      end
    end
  end


  def self.need_fiscal
    all.where("fiscal_phone_count < sections_count")
  end

  def self.need_fiscal_in
    all.where("fiscal_phone_in_count < sections_count")
  end

  def check_fiscals_in
    counter = fiscal_phones.where("last_location_success > ?", Time.now - 10.minutes).count
    update(fiscal_phone_in_count: counter)
  end
  
end
