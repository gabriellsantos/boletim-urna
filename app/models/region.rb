# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  partner_id :integer
#
# Indexes
#
#  index_regions_on_partner_id  (partner_id)
#
# Foreign Keys
#
#  fk_rails_...  (partner_id => partners.id)
#

class Region < ApplicationRecord
  belongs_to :partner
  has_one :candidate, through: :partner
  has_many :region_cities
  has_many :cities, through: :region_cities

  accepts_nested_attributes_for :region_cities, allow_destroy: true
end
