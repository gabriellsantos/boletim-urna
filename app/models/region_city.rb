# == Schema Information
#
# Table name: region_cities
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city_id    :integer
#  region_id  :integer
#
# Indexes
#
#  index_region_cities_on_city_id    (city_id)
#  index_region_cities_on_region_id  (region_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (region_id => regions.id)
#

class RegionCity < ApplicationRecord
  belongs_to :region
  belongs_to :city
end
