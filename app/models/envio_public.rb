# == Schema Information
#
# Table name: envio_publics
#
#  id               :integer          not null, primary key
#  code             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  bulletin_id      :integer
#  fiscal_public_id :integer
#
# Indexes
#
#  index_envio_publics_on_bulletin_id       (bulletin_id)
#  index_envio_publics_on_fiscal_public_id  (fiscal_public_id)
#
# Foreign Keys
#
#  fk_rails_...  (bulletin_id => bulletins.id)
#  fk_rails_...  (fiscal_public_id => fiscal_publics.id)
#

class EnvioPublic < ApplicationRecord
  require "uniquify"
  belongs_to :fiscal_public
  belongs_to :bulletin

  uniquify :code, length: 6, chars: 0..9
end
