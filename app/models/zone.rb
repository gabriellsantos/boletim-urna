# == Schema Information
#
# Table name: zones
#
#  id         :integer          not null, primary key
#  number     :string
#  number_i   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Zone < ApplicationRecord
  has_many :zone_cities
  has_many :cities, through: :zone_cities
  has_many :polling_places
  has_many :sections, through: :polling_places

  def as_json(options = {})
    s = super(options)
    s[:locais] = self.polling_places
    s
  end

  def to_s
    self.number
  end
  def self.set_number_i
    all.map do |zone|
      zone.number_i = zone.number.to_i
      zone.save
    end
  end
end
