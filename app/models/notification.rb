# == Schema Information
#
# Table name: notifications
#
#  id          :integer          not null, primary key
#  description :string
#  fiscal      :string
#  lida        :boolean
#  photo       :string
#  sended      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  partner_id  :integer
#  user_id     :integer
#
# Indexes
#
#  index_notifications_on_partner_id  (partner_id)
#  index_notifications_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (partner_id => partners.id)
#  fk_rails_...  (user_id => users.id)
#

class Notification < ApplicationRecord
  belongs_to :partner, optional: true
  belongs_to :fiscal_phone, foreign_key: :fiscal, primary_key: :secrete
  belongs_to :user, optional: true

  mount_base64_uploader :photo, AvatarUploader

  default_scope{ order(id: :desc)}

  after_create :send_notification

  def send_notification
    if self.sended
      OneSignal::OneSignal.api_key = "NWU2ZTJlNjMtNWZlNC00YmYyLWI3ZGItNjA4ZGNiMWE1Njkz"#"Y2JhNGY0ODctZjE1Yy00YzI2LWJiMDAtYWE0MDEzNDdmMDQx"
      filters = [
          {"field": "tag", "key": "apuracao_partner_#{self.partner_id}", "relation": "=", "value": 'enable' }
      ]

      params = {
          app_id: "9040aa87-8001-4a25-a80b-9dbc42b38e3f",#"c75ccc73-2dc7-4a2b-b6b8-a7430301f843"
          filters: filters,
          contents: {en: self.description}

      }

      OneSignal::Notification.create(params: params)

    end
  end

  def as_json(options = {})
    s = super(options)
    s[:enviado_por] = self.fiscal_phone.present? ? self.fiscal_phone.name : self.user.name
    s[:telefone] = self.fiscal_phone.present? ? self.fiscal_phone.phone : ""
    s
  end

end
