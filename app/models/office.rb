# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  brancos    :integer
#  legenda    :integer
#  name       :string
#  nominais   :integer
#  nulos      :integer
#  numero     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Office < ApplicationRecord
  has_many :candidates
  has_many :votes
  has_many :office_cities
  has_many :cities, through: :office_cities

  def to_s
    self.numero.to_s
  end

  def votos_computados
    (brancos || 0) + (nominais || 0) + (nulos || 0)
  end

  def as_json(options = {})
    s = super(options)
    s[:candidatos] = self.candidates.order(votos: :desc)
    s[:pleito] = Litigation.last
    s[:votos_computados] = self.votos_computados
    s[:cidades] = JSON.parse(self.office_cities.to_json)
    s
  end
end
