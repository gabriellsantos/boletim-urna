# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  candidato  :integer
#  municipio  :integer
#  pleito     :integer
#  processo   :integer
#  secao      :integer
#  turno      :integer
#  urna       :integer
#  votos      :integer
#  zona       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city_id    :integer
#  office_id  :integer
#  section_id :integer
#  zone_id    :integer
#
# Indexes
#
#  index_votes_on_city_id     (city_id)
#  index_votes_on_office_id   (office_id)
#  index_votes_on_section_id  (section_id)
#  index_votes_on_zone_id     (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (office_id => offices.id)
#  fk_rails_...  (section_id => sections.id)
#  fk_rails_...  (zone_id => zones.id)
#

class Vote < ApplicationRecord
  belongs_to :office, optional: true
  belongs_to :candidate, foreign_key: :candidato, optional: true
  belongs_to :city, optional: true
  belongs_to :zone, optional: true
  belongs_to :section, optional: true

  after_create :atualiza_votos

  def atualiza_votos
    candidato = Candidate.find_by_numero(self.candidato)
    begin
      candidato.votos  += self.votos

    rescue

    end

    if candidato
      candidato.save
    end
  end
end
