# == Schema Information
#
# Table name: fiscal_publics
#
#  id         :integer          not null, primary key
#  email      :string
#  name       :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FiscalPublic < ApplicationRecord
	has_many :envio_publics, dependent: :destroy
end
