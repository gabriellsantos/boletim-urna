# == Schema Information
#
# Table name: sections
#
#  id               :integer          not null, primary key
#  address          :string
#  latitude         :string
#  longitude        :string
#  name             :string
#  namn             :string
#  number           :string
#  uid              :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  city_id          :integer
#  polling_place_id :integer
#
# Indexes
#
#  index_sections_on_city_id           (city_id)
#  index_sections_on_polling_place_id  (polling_place_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (polling_place_id => polling_places.id)
#

class Section < ApplicationRecord
  belongs_to :polling_place

  def to_s
    self.number
  end

  def self.define_uid
    all.map do |section|
      begin
        section.update(uid: "z#{section.zone.number_i}s#{section.number}")
      rescue
        puts section.id
      end
    end
  end

  def zone
    polling_place.zone
  end

  def self.remove_duplicates
    all.group_by(&:uid).map do |dup|
      begin
        dup[1].pop #leave one
        dup[1].map{|a| a.destroy}
      rescue
        puts dup[0]
      end
    end
  end
end
