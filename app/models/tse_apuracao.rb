class TseApuracao

  def self.host
    "http://divulga.tse.jus.br/2018/divulgacao/oficial"
  end

  def self.get_data(office, state, election)
    url = "#{host}/#{election}/dadosdivweb/#{state}/#{state}-c#{office.rjust(4, '0')}-e#{election.rjust(6, '0')}-w.js?#{Time.now.to_i}"
    response = RestClient.get(url)
    JSON.parse(response)
  end

  def self.send_to_firebase(office="3", state="to", election="305")
    data = get_data(office, state, election)

    sort_cand = data["cand"].sort_by { |hash| hash['seq'].to_i }

    data[:cand] = sort_cand

    firebase_url = "https://notificato-seplan.firebaseio.com/tse"
    firebase_secret = "qm4kJVOdqS4sjVVOkA2tbVC5qpIRjd9OCN5Alrs1"

    firebase = Firebase::Client.new(firebase_url, firebase_secret)

    firebase.update("o#{office}s#{state}e#{election}", data)
  end
end