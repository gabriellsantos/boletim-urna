# == Schema Information
#
# Table name: litigations
#
#  id         :integer          not null, primary key
#  eleitores  :integer
#  faltantes  :integer
#  numero     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Litigation < ApplicationRecord

  require 'rest-client'

  def votos_validos
    (eleitores || 0) - (faltantes || 0)
  end

  def self.getDados
    response = RestClient.get 'http://qrcodenobu.tse.jus.br/json-bu/oficial/226/o00232to73008-qbu.js'
    JSON.parse(response)
  end

  def self.importar_zonas
    response = RestClient.get 'http://divulga.tse.jus.br/2018/divulgacao/oficial/304/config/to/to-e000304-w.js'
    dados = JSON.parse(response)
    dados.each do |d|
      m = City.find_or_create_by(number: d["cd"])
      m.name = d["nm"]
      m.sede = d["c"] == "S" ? true : false
      m.save
      d["z"].each do |z|
        zona = Zone.find_or_create_by(number: z)
        zc = ZoneCity.find_or_create_by(city_id: m.id, zone_id: zona.id)
      end
    end
  end
end
