# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string
#  number     :string
#  sede       :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class City < ApplicationRecord
  has_many :zone_cities
  has_many :zones, through: :zone_cities
  has_many :votes

  def to_s
    self.name
  end

  def as_json(options = {})
    s = super(options)
    s[:votos] = self.votes
    s
  end
end
