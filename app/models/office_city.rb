# == Schema Information
#
# Table name: office_cities
#
#  id         :integer          not null, primary key
#  brancos    :integer
#  legenda    :integer
#  nominais   :integer
#  nulos      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city_id    :integer
#  office_id  :integer
#
# Indexes
#
#  index_office_cities_on_city_id    (city_id)
#  index_office_cities_on_office_id  (office_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (office_id => offices.id)
#

class OfficeCity < ApplicationRecord
  belongs_to :office
  belongs_to :city
  has_many :votes, through: :city

  def as_json(options = {})
    s = super(options)
    s[:votos] = self.votes
    s
  end
end
