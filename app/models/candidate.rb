# == Schema Information
#
# Table name: candidates
#
#  id         :integer          not null, primary key
#  foto       :string
#  name       :string
#  numero     :integer
#  votos      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  office_id  :integer
#
# Indexes
#
#  index_candidates_on_office_id  (office_id)
#
# Foreign Keys
#
#  fk_rails_...  (office_id => offices.id)
#

class Candidate < ApplicationRecord
  belongs_to :office, optional: true
  has_many :votes, foreign_key: :candidato, primary_key: :numero

  mount_uploader :foto, AvatarUploader

  before_create :setVoto

  def setVoto
  	self.votos = 0
  end

  def to_s
    self.name
  end

end
