require 'sidekiq/web'
Rails.application.routes.draw do
  get 'home/index'

  get 'tse/apuracao'

  resources :polling_places do
    collection do
      get "by_city"
      get "dashboard"
    end
    member do
      get "set_phone"
    end
  end
  resources :fiscal_publics do
    collection do
      get "autorizar"
    end
  end
  resources :regions
  resources :sections
  resources :cities
  resources :zones
  resources :notifications do
    collection do
      post "receber"
      get "ler"
    end

  end

  resources :partners do
    collection do
      get "aparelhos"
      get "novo_aparelho"
      get "ativar_aparelho"
      get "manual"
      get "youtubeVideo"
      get "ativar_fiscal"
      get "retornaParceiros"
      get "verificar_fiscal"
      get "verificar_fiscal_publico"
      get "termos_condicoes"
      get "set_polling_place"
      get "teste"
      get "atualiza_fiscal"
      get "location"
    end
  end
  devise_for :users
  resources :users
  resources :litigations
  resources :votes
  resources :bulletins
  resources :candidates
  resources :offices do
    collection do
      get "receber"
      get "teste"
    end
    member do
      get "apuracao"
    end
  end

  devise_scope :user do
    authenticated :user, lambda { |u| u.admin? } do
      mount Sidekiq::Web => '/processos'
    end
  end

  root "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
