app.controller("firebase_controller", function($scope, $firebaseObject) {
    var ref = firebase.database().ref();//child("apuracao")
    // download the data into a local object
    //$scope.data = $firebaseObject(ref);
    // putting a console.log here won't work, see below
    $scope.cargo = "";
    $scope.eleito = false;
    $scope.segundoturno = false;
    $scope.metade = 0;
    $scope.votos = 0;
    ref.on("value", function(snapshot) {
        // This isn't going to show up in the DOM immediately, because
        // Angular does not know we have changed this in memory.
        // $scope.data = snapshot.val();
        // To fix this, we can use $scope.$apply() to notify Angular that a change occurred.
        $scope.$apply(function() {
            $scope.data = snapshot.val();
            $scope.metade = ($scope.data.apuracao[$scope.cargo].pleito.eleitores - ($scope.data.apuracao[$scope.cargo].brancos + $scope.data.apuracao[$scope.cargo].nulos + $scope.data.apuracao[$scope.cargo].pleito.faltantes))/2
            if($scope.data.apuracao[$scope.cargo].candidatos[0].votos >= $scope.metade+1)
            {
                $scope.eleito = true;
            }
            $scope.votos = $scope.data.apuracao[$scope.cargo].candidatos.reduce(function(acc,val){
               return acc + val.votos;
            },0);
            if(($scope.votos - $scope.data.apuracao[$scope.cargo].candidatos[0].votos) >= $scope.metade+1)
            {
                $scope.segundoturno = true;
            }
        });
    });

    $scope.arredondar = function(value)
    {
        return Math.round(value);
    }

    $scope.$watch('data', function() {
        if($scope.data)
        {
            var dados = $scope.data.apuracao[""+$scope.cargo].candidatos.map(function(c){
               return [c.name, c.votos];
            });
            //console.log(dados);
            drawChart(dados)
        }
    });
});