app.controller("notification_controller", function($scope, $http)
{
    $scope.mensagens_recebidas = [];
    $scope.mensagens_enviadas = [];
    $scope.mensagens = [];
    $scope.mensagem_selecionada = '';

    $scope.tipo_mensagem = function(tipo)
    {
        $scope.mensagem_selecionada = "";
        if(tipo == 1)
        {
            $scope.mensagens = $scope.mensagens_recebidas;
            ativa(tipo);
        }
        else
        {
            $scope.mensagens = $scope.mensagens_enviadas;
            ativa(tipo);
        }
    }

    $scope.seleciona_mensagem = function(msg)
    {
        $scope.mensagem_selecionada = msg;
        seleciona(msg.id);
        $http.get("/notifications/ler?msg="+msg.id).then(function success(data){

        },function error(){

        });
    }

    $scope.$watch('mensagens_recebidas', function() {
        if($scope.mensagens_recebidas)
        {
           $scope.mensagens = $scope.mensagens_recebidas;
        }
    });
});