app.controller("tse_oficial_controller", function($scope, $firebaseObject) {
    var ref = firebase.database().ref();//child("apuracao")

    $scope.cargo = "";
    $scope.eleito = false;
    $scope.segundoturno = false;
    $scope.metade = 0;
    $scope.votos = 0;
    $scope.dados = {};
    ref.on("value", function(snapshot) {

        $scope.$apply(function() {
            $scope.data = snapshot.val();
            $scope.dados = $scope.data.tse.o3stoe305;
            $scope.metade = $scope.dados.vv /2;
            $scope.eleito = $scope.dados.cand[0].e == 's' && $scope.dados.cand[1].e == 'n';
            $scope.segundoturno = $scope.dados.cand[1].e == 's'
        });
    });

    $scope.arredondar = function(value)
    {
        return Math.round(value);
    }

    $scope.arredondar_2 = function(value)
    {
        return value.toFixed(2);
    }
});